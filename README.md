# README #

Sample Unity3D application to be used inside a Webview on a SAGE2 display wall.

### What is this repository for? ###

* For developers of a Unity3D application who want to show it on a SAGE2 wall.
* It demonstrates how to get events and interaction properly setup for SAGE2.

### How do I get set up? ###

* Can be used as any SAGE2 application. Zip your WebGL output folder and drag-drop onto the SAGE2 UI.
* See: https://bitbucket.org/sage2/sage2/wiki/SAGE2%20Unity%20Applications

### module-sage2 (Experimental) ###

* Assets/module-sage2 is a plugin specfically for connecting your Unity application to SAGE2 for more advanced SAGE2 integration beyond just a WebGL Webview.
* This allows your Unity application to be aware of the size and state of the SAGE2 wall.
	* This includes the position of other SAGE2 applications on the wall, all SAGEPointers, etc.
* The examples in Assets/module-sage2/scenes:
	* Display Client.unity
		* A version of the SAGE2 display inside a Unity canvas object.
	* World Space.unity
		* A version of the SAGE2 display inside a Unity canvas object, but in world coordinates.
		* Useful for adding a SAGE2 wall into a 3D scene using actual server data.
	* SAGE UI.unity
		* A version of the SAGE2 UI inside a Unity canvas object.
		* Displays like the SAGE2 web ui, but currently non-interactable.
	* ObserverOnly.unity
		* A basic Unity-SAGE2 server example to get application position/size and SAGEPointer data without rendering in Unity.
* The plugin supports Websocket connections from the Unity editor or standalone application (C#), Universal Windows Platform (UWP) build, or HTML5 (Javascript/jslib) builds.
