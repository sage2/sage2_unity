﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField]
    GameObject[] projectile;

    [SerializeField]
    float cannonForce = 10;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        transform.LookAt(ray.GetPoint(100));

        if (Input.GetMouseButton(0))
        {
            int random = (int)Random.Range(0, projectile.Length);
            GameObject p = Instantiate(projectile[random], transform.position, transform.rotation) as GameObject;
            p.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * cannonForce);
        }
    }
}