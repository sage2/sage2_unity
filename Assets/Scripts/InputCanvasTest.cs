﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputCanvasTest : MonoBehaviour {

    [SerializeField]
    Text buttonLog;

    [SerializeField]
    Text axisText;

    string inputString;

    [SerializeField]
    bool showButtonHolds = false;

    public void ShowButtonHoldEvents(bool val)
    {
        showButtonHolds = val;
    }

    // Use this for initialization
    void Start () {
        inputString = "";

    }

    // Update is called once per frame
    void Update() {
        if (true)
        {
            foreach (KeyCode kcode in KeyCode.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(kcode))
                {
                    inputString += "Input.GetKeyDown: " + kcode + "\n";
                }
                if (showButtonHolds && Input.GetKey(kcode))
                {
                    inputString += "Input.GetKey: " + kcode + "\n";
                }
                if (Input.GetKeyUp(kcode))
                {
                    inputString += "Input.GetKeyUp " + kcode + "\n";
                }
            }

            if (Input.GetButtonDown("Horizontal"))
            {
                inputString += "InputManager: GetButtonDown Horizontal" + "\n";
            }
            if (Input.GetButtonDown("Vertical"))
            {
                inputString += "InputManager: GetButtonDown Vertical" + "\n";
            }
            if (Input.GetButtonDown("Fire1"))
            {
                inputString += "InputManager: GetButtonDown Fire1" + "\n";
            }
            if (Input.GetButtonDown("Fire2"))
            {
                inputString += "InputManager: GetButtonDown Fire2" + "\n";
            }
            if (Input.GetButtonDown("Fire3"))
            {
                inputString += "InputManager: GetButtonDown Fire3" + "\n";
            }
            if (Input.GetButtonDown("Jump"))
            {
                inputString += "InputManager: GetButtonDown Jump" + "\n";
            }
            if (Input.GetButtonDown("Submit"))
            {
                inputString += "InputManager: GetButtonDown Submit" + "\n";
            }
            if (Input.GetButtonDown("Cancel"))
            {
                inputString += "InputManager: GetButtonDown Cancel" + "\n";
            }

            if (showButtonHolds)
            {
                if (Input.GetButton("Horizontal"))
                {
                    inputString += "InputManager: GetButton Horizontal" + "\n";
                }
                if (Input.GetButton("Vertical"))
                {
                    inputString += "InputManager: GetButton Vertical" + "\n";
                }
                if (Input.GetButton("Fire1"))
                {
                    inputString += "InputManager: GetButton Fire1" + "\n";
                }
                if (Input.GetButton("Fire2"))
                {
                    inputString += "InputManager: GetButton Fire2" + "\n";
                }
                if (Input.GetButton("Fire3"))
                {
                    inputString += "InputManager: GetButton Fire3" + "\n";
                }
                if (Input.GetButton("Jump"))
                {
                    inputString += "InputManager: GetButton Jump" + "\n";
                }
                if (Input.GetButton("Submit"))
                {
                    inputString += "InputManager: GetButton Submit" + "\n";
                }
                if (Input.GetButton("Cancel"))
                {
                    inputString += "InputManager: GetButton Cancel" + "\n";
                }
            }

            if (Input.GetButtonUp("Horizontal"))
            {
                inputString += "InputManager: GetButtonUp Horizontal" + "\n";
            }
            if (Input.GetButtonUp("Vertical"))
            {
                inputString += "InputManager: GetButtonUp Vertical" + "\n";
            }
            if (Input.GetButtonUp("Fire1"))
            {
                inputString += "InputManager: GetButtonUp Fire1" + "\n";
            }
            if (Input.GetButtonUp("Fire2"))
            {
                inputString += "InputManager: GetButtonUp Fire2" + "\n";
            }
            if (Input.GetButtonUp("Fire3"))
            {
                inputString += "InputManager: GetButtonUp Fire3" + "\n";
            }
            if (Input.GetButtonUp("Jump"))
            {
                inputString += "InputManager: GetButtonUp Jump" + "\n";
            }
            if (Input.GetButtonUp("Submit"))
            {
                inputString += "InputManager: GetButtonUp Submit" + "\n";
            }
            if (Input.GetButtonUp("Cancel"))
            {
                inputString += "InputManager: GetButtonUp Cancel" + "\n";
            }


        }

        if (Input.mouseScrollDelta.magnitude != 0)
        {
            inputString += "Input.mouseScrollDelta: " + Input.mouseScrollDelta + "\n";
        }

        if (buttonLog)
        {
            buttonLog.text = inputString;
            if( inputString.Length > 10000)
            {
                inputString = inputString.Substring(inputString.Length / 2, inputString.Length / 2);
            }
        }
        if (axisText)
        {
            string axisString = "";
            axisString += "InputManager: Horizontal: " + Input.GetAxis("Horizontal") + "\n";
            axisString += "InputManager: Vertical: " + Input.GetAxis("Vertical") + "\n";
            axisString += "InputManager: Mouse X: " + Input.GetAxis("Mouse X") + "\n";
            axisString += "InputManager: Mouse Y: " + Input.GetAxis("Mouse Y") + "\n";
            axisString += "InputManager: Mouse ScrollWheel: " + Input.GetAxis("Mouse ScrollWheel") + "\n";

            axisString += "InputManager: Mouse ScrollWheel: " + Input.GetAxis("Mouse ScrollWheel") + "\n";

            axisString += "Input.mousePosition: " + Input.mousePosition + "\n";

            if (axisText)
                axisText.text = axisString;
        }


	}
}
