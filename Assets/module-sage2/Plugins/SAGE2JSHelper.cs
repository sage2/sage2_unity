// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2018

using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>Helper class for accessing Javascript in WebGL builds
/// </summary>
public static class SAGE2JSHelper {
#if UNITY_WEBGL || UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern string GetCookieJS(string name);
#endif
    public static string GetCookie(string name)
	{
#if UNITY_WEBGL && !UNITY_EDITOR
        return GetCookieJS(name);
#else
        Debug.LogWarning("SAGE2JSHelper:GetCookie() only works in WebGL builds");
        return "";
#endif
    }
}