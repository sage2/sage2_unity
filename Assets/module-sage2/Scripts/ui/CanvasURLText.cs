﻿using UnityEngine.UI;
using UnityEngine;

public class CanvasURLText : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
		if(text)
        {
            if(Application.absoluteURL.Length > 0 )
                text.text = Application.absoluteURL;
            else
                text.text = "App URL Not Available";
        }
	}
}
